package services

import (
  "log"
  "gitlab.com/qventura-portfolio/configurations-service.git/internal/models"
)

type ConfigurationsService struct {
  Context models.RequestContext
}

func (s ConfigurationsService) GetAll() []models.Configuration {
  log.Print("configurationService.GetAll")
  result := make([]models.Configuration, 2)

  result[0] = models.Configuration{Key: "DatabaseUrl", Value: "test"}
  result[1] = models.Configuration{Key: "SomethingElse", Value: "test"}

  return result
}

func (s ConfigurationsService) GetOne(key string) models.Configuration {
  log.Print("configurationService.GetOne")

  return models.Configuration{Key: "DatabaseUrl", Value: "test"}
}
