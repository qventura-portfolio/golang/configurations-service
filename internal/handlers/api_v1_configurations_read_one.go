package handlers

import (
  "log"
  "fmt"
  "github.com/aws/aws-lambda-go/events"
  "gitlab.com/qventura-portfolio/configurations-service.git/internal/services"
)

var ApiV1ConfigurationsReadOne = func(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
  log.Print("apiV1ConfigurationReadOne.Handler")
  fmt.Printf("%+v\n", request)

  configuration := services.
    ConfigurationsService{Context: BuildRequestContext(request)}.
    GetOne(request.PathParameters["id"]);

	return events.APIGatewayProxyResponse{
      Body: BuildResponseBody("configurations", configuration),
      StatusCode: 200,
    },
    nil
}
