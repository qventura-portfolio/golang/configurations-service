package handlers

import (
  "log"
  "fmt"
  "github.com/aws/aws-lambda-go/events"
  "gitlab.com/qventura-portfolio/configurations-service.git/internal/services"
)

var ApiV1ConfigurationsReadAll = func(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
  log.Print("ApiV1ConfigurationsReadAll.Handler")
  fmt.Printf("%+v\n", request)

  configurations := services.
    ConfigurationsService{Context: BuildRequestContext(request)}.
    GetAll()

	return events.APIGatewayProxyResponse{
      Body: BuildResponseBody("configurations", configurations),
      StatusCode: 200,
    },
    nil
}
