package main

import (
  "log"
  "github.com/aws/aws-lambda-go/lambda"
)

import "gitlab.com/qventura-portfolio/configurations-service.git/internal/handlers"

func main() {
  log.Print("api_v1_configuration_read_one.main")
	lambda.Start(handlers.ApiV1ConfigurationsReadOne)
}
