package models

type RequestContext struct {
  Stage string
  RequestID string
}
