module gitlab.com/qventura-portfolio/configurations-service.git

go 1.12

require (
	github.com/aws/aws-lambda-go v1.11.1
	github.com/aws/aws-sdk-go v1.20.12
	github.com/google/pprof v0.0.0-20190515194954-54271f7e092f // indirect
	github.com/ianlancetaylor/demangle v0.0.0-20181102032728-5e5cf60278f6 // indirect
	golang.org/x/arch v0.0.0-20190312162104-788fe5ffcd8c // indirect
	golang.org/x/crypto v0.0.0-20190621222207-cc06ce4a13d4 // indirect
	golang.org/x/sys v0.0.0-20190626221950-04f50cda93cb // indirect
	golang.org/x/tools v0.0.0-20190628222527-fb37f6ba8261 // indirect
)
