.PHONY: build clean deploy

build:
	env GOOS=linux GO111MODULE=on go build -ldflags '-d -s -w' -a -tags netgo -installsuffix netgo -o bin/api_v1_configurations_read_all cmd/api_v1_configurations_read_all/main.go
	env GOOS=linux GO111MODULE=on go build -ldflags '-d -s -w' -a -tags netgo -installsuffix netgo -o bin/api_v1_configurations_read_one cmd/api_v1_configurations_read_one/main.go

clean:
	rm -rf ./bin

deploy: clean build
	sls deploy --verbose
