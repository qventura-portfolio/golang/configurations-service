package handlers

import (
  "encoding/json"
  "log"
)

var BuildResponseBody = func(rootKey string, value interface{}) string {
  body := make(map[string]interface{})
  body[rootKey] = value
  jsonBody, err := json.Marshal(body)

  if err != nil {
    log.Println(err)
  }

  return string(jsonBody)
}
