package handlers

import (
  "github.com/aws/aws-lambda-go/events"
  "gitlab.com/qventura-portfolio/configurations-service.git/internal/models"
)

var BuildRequestContext = func(request events.APIGatewayProxyRequest) models.RequestContext {
  lambdaRequestContent := request.RequestContext;
	return models.RequestContext{Stage: lambdaRequestContent.Stage, RequestID: lambdaRequestContent.RequestID}
}
